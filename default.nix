# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  boot = {
    loader.timeout = 2;
    tmpOnTmpfs = true;
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  environment.pathsToLink = [ "/share/zsh" ];

  # Open ports in the firewall.
  networking = {
    firewall = {
      allowedTCPPorts = [ ];
      allowedUDPPorts = [ ];
    };
    networkmanager = {
      insertNameservers =
        [
          "1.1.1.1"
          "1.0.0.1"
        ];
      dns = "none";
    };
  };

  hardware = {
    pulseaudio.enable = true;

    opengl.driSupport32Bit = true;
    pulseaudio.support32Bit = true;
  };

  programs.slock.enable = true;

  services = {
    # Enable the OpenSSH daemon.
    openssh.enable = false;

    # Necessary for home-manager to handle gtk themes
    dbus.packages = with pkgs; [ gnome3.dconf ];


    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      layout = "fr";
      xkbVariant = "bepo";

      displayManager = {
        defaultSession = "home-manager";

        lightdm = {
          autoLogin = {
            enable = true;
            user = "ivann";
          };
        };
      };

      desktopManager = {
        wallpaper.mode = "fill";

        session = [
          {
            name = "home-manager";
            start = ''
              ${pkgs.runtimeShell} $HOME/.xsession &
              waitPID=$!
            '';
          }
        ];
      };
    };

    # Make udisks2 mount to /media instead of /run/media/user/
    udev.extraRules = ''
      ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
    '';

    # unbound.enable = true;

    xbanish.enable = true;
  };

  systemd.services = {
    lockonsuspend = {
      wantedBy = [ "sleep.target" ];
      before = [ "sleep.target" ];
      script = "/run/wrappers/bin/slock";
      postStart = "${pkgs.coreutils}/bin/sleep 1";
    };
  };

  fileSystems."/media" = {
    fsType = "tmpfs";
    options = [ "size=1M" ];
  };

  virtualisation = {
    docker.enable = true;
    virtualbox.host.enable = true;
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?
}
