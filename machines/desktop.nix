{ config, lib, pkgs, ... }:

{
  imports =
    [ <nixpkgs/nixos/modules/installer/scan/not-detected.nix>
      ../default.nix
      ../users.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usb_storage" "usbhid" "sd_mod" ];
      kernelModules = [ "dm-snapshot" "amdgpu" ];
      luks.devices = {
        root = {
          device = "/dev/disk/by-uuid/45940249-dfba-4f19-b038-7fc0dbe58dae";
          preLVM = true;
          allowDiscards = true;
        };
      };
    };

    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "kvm-amd" ];
    extraModulePackages = [ ];
  };

  networking = {
    hostId = "b356f935";
    hostName = "desktop";
    useDHCP = false;
    networkmanager.enable = true;
    extraHosts = "
      192.168.0.50 server
      ";
  };

  hardware = {
    cpu.amd.updateMicrocode = true;
  };

  services = {
    xserver = {
      videoDrivers = [ "amdgpu" ];
      deviceSection = ''
        Option "TearFree" "true"
      '';
    };

    fstrim.enable = true;
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/9583-0183";
      fsType = "vfat";
    };

    "/" = {
      device = "/dev/disk/by-uuid/e6364417-8198-4177-bf0f-2583b4db24ae";
      fsType = "ext4";
      options = [ "noatime" "nodiratime" ];
    };

    "/home" = {
      device = "/dev/disk/by-uuid/758d4bc1-e041-4491-9c1f-566cea91c436";
      fsType = "ext4";
      options = [ "noatime" "nodiratime" ];
    };

    "/mnt/server" = {
      device = "server:/";
      fsType = "nfs";
      options = [ "x-systemd.automount" "noauto" ];
    };
  };

  swapDevices = [ {
    device = "/dev/disk/by-uuid/ee033966-8c2e-4400-abf4-7504c3629f0f";
  } ];

  services.xserver.xrandrHeads = [
    "HDMI-A-0"
    { output = "DisplayPort-0";
      primary = true;
    }
  ];

  nix.maxJobs = lib.mkDefault 32;
}
